<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">
    <title>Sign up to Attract!</title>
    
</head>
<body>
    <header>
        <div class="logo">
          <img src="assets/LOGO Doang.png" alt="Your Logo">
        </div>
        <div class="header-text">
          <h1 class="logo-text">Attract!</h2>
        </div>
    </header>
    <div class="wrapper text-center">
        <h1 class="login-title">Sign up to <span class="logo-title">Attract!</span></h1>
        <form action="/signup" method="post">
            @csrf
            <label class="label-text">Username</label>
            <input type="text" name="username">
            <label class="label-text">Password</label>
            <input type="password" name="password">
            <button type="submit">Create!</button>
        </form>
        <div class="create">
            I already have an account, <a href="{{route('login.index')}}">Log in!</a>
        </div>
    </div>
</body>
</html> 