<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.js"></script>
    <title>Create Activity</title>
    <link rel="stylesheet" href="/css/category/Activity-modify.css">
    <link rel="stylesheet" href="/css/category/Activity-view.css">
    <link rel="stylesheet" href="/css/category/Sidebar.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="sidebar" id="sidebar">
        <div class="main-option">
            <div class="title"><h2>Menu</h2>
                <button onclick="ToggleSidebar(false)">
                    <i class= "fa-solid fa-xmark sidebar-logo"></i>
                </button>
            </div>
            <div id="search-results">
            <form action="{{ route('activities.search') }}" method="GET" id="search-form">
                <input type="text" id="search-input" name="search" placeholder="Search">
                <button type="submit">Search</button>
            </form>
            </div>
            <div class="list-container">
                <div class="list " id = tasks>
                    <h5>TASKS</h5>
                    <ul>
                        <li class="choice">
                            <a href="{{ route('show-activities-by-time', 'Upcoming') }}">
                                <i class="fa-solid fa-angles-right choice-logo"></i>
                                <span>Upcoming</span>
                            </a>
                        </li>
                        <li class="choice">
                            <a href="{{ route('show-activities-by-time', 'Today') }}">
                                <i class="fa-solid fa-list-check choice-logo"></i>
                                <span>Today</span>
                            </a>
                        </li>
                        <li class="choice">
                            <a href="{{ route('show-activities-by-time', 'This Week') }}">
                                <i class="fa-solid fa-list-check choice-logo"></i>
                                <span>This Week</span>
                            </a>
                        </li>
                        <li class="choice" onclick="redirectToAllCategories()"><span>Category</span></a></li>
                    </ul>
                </div>
                <div class="list" id="categories">
                <h5>CATEGORIES</h5>
                <ul id="category-list">
                    @if(isset($categories) && count($categories) > 0)
                        @foreach ($categories as $category)
                        <li class="choice category_choice" data-category-id="{{ $category->id }}">
                        <span class="category-name">
                        <span class="category-text">{{ $category->category }}</span>
                        <input type="text" class="edit-category" value="{{ $category->category }}" style="display:none;">
                        </span>
                        </li>
                        @endforeach
                    @else
                        <li id="no-categories-message">No categories found</li>
                    @endif
                    <li id="add-category" class="choice">
                            <div id="add-category-container" class="category-name" onclick="addCategory()">
                                <i class="fa-solid fa-plus choice-logo"></i>
                                <span id="add-category-text">Add New Category</span>
                            </div>
                        <input type="text" id="add-category-input" style="display:none;">
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <form action="/logout" method="POST" id="signoutForm">
            @csrf
            <div class="sign-out choice">
                <i class="fa-solid fa-right-from-bracket  sign-out-logo"></i><span>Sign Out</span>
            </div>
        </form>
    </div>
    <div class="menubar">
        <button onclick="ToggleSidebar(true)">
            <i class="fa-solid fa-bars"></i>
        </button>
    </div>
    <div id="activity-view">
    <div class="header-view">
        <h1>All Categories and Activities</h1>
    </div>
    <div class="return">
        <h2><a href="/">Back to main page</a></h2>
    </div>
    <div class="task-list-container" id="today-list">
        @forelse ($categories as $category)
            <h2>{{ $category->category }}</h2>
            @if(count($category->activities) > 0)
                <ul class="task-list">
                    @foreach ($category->activities as $activity)
                        <li>
                            <div class="task">
                                <form action="/toggle-done" method="get" class="done-form">
                                    <section>
                                        <input type="hidden" name="id" value="{{$activity->id}}">
                                        <input type="checkbox" name="checkbox" class="done-checkbox"
                                            @if ($activity->completion_status)
                                                checked
                                            @endif
                                        >
                                        <h3 class="task-title">{{$activity->header}}</h3>
                                    </section>
                                </form>
                                <section>
                                    <button class="" onclick="DisplayEditForm({{$activity->id}})">
                                        <i class="fa-solid fa-chevron-right expand-task"></i>
                                    </button>
                                </section>
                            </div>
                            <div class="seperator"></div>
                        </li>
                    @endforeach
                </ul>
            @else
                <p>No activities in this category.</p>
            @endif
        @empty
            <p>No categories found.</p>
        @endforelse
    </div>
</div>

    <div id="activity-modify">
        <div class="input">
            <div class="main-input">
                <div class="header-task">
                    <h2>Task:</h2>
                    <button onclick="ToggleFormView(false, false)">
                        <i class="fa-solid fa-xmark sidebar-logo"></i>
                    </button>
                </div>
                <form action="" method="post" id="edit-activity-forum">
                    @csrf
                    @method('put')
                    <input type="hidden" id="activity-id-edit-form">

                    <div class="input-container">
                        <input name="edit-header" type="text" placeholder="Title" id="header-edit-field" readonly>
                    </div>
                    <div class="input-container">
                        <textarea name="edit-content" rows="5" placeholder="Description" id="content-edit-field" readonly></textarea>
                    </div>
                    <div class="choice-container">
                    <h4>Category</h4>
                    <select name="category" id="category-input" readonly>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="choice-container">
                        <h4>Due Date</h4>
                        <input name="edit-deadline" type="datetime-local" id="date-edit-field" readonly>
                    </div>
                </form>
            </div>
            
            <div class="sub-task-container">
                <h2>Sub Task</h2>
                <div class="add-new-sub-task">
                    <i class="fa-solid fa-plus logo-add-new-task"></i>
                    <h3>Add New Task</h3>
                </div>
                <div class="seperator"></div>
                <ul>
                    <li>
                        <div class="sub-task">
                            <section class="sub-task">
                                <input type="checkbox" name="" id="">
                                <h3 class="task-title">Research content ideas</h3>
                            </section>
                            <section class="sub-task">
                                <button class="">
                                    <i class="fa-solid fa-ellipsis-vertical expand-sub-task">
                                        <div class="sub-task-menu">
                                            <h5 id="edit-sub-task">Edit</h5>
                                            <h5 id="delete-sub-task">Delete</h5>
                                        </div>
                                    </i>
                                </button>
                            </section>
                        </div>
                        <div class="sub-task-input">
                            <input type="text" placeholder="Title">
                        </div>
                        <div class="seperator"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="submit-form">
            <button form ="edit-activity-forum" id="delete" onclick="submitForm('delete')">Delete Task</button>
            <button form = "edit-activity-forum" id="edit" onclick="submitForm('edit')">Save Changes</button>
            <button form = "edit-activity-forum" id="create" onclick="submitForm('create')">Create Activity</button>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="/js/dateFormat.js"></script>
    <script src="/js/edit-activity.js"></script>
    <script src="/js/main_activity.js"></script>
</body>
</html>
