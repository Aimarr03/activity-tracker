<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.js"></script>
    <title>Create Activity</title>
</head>
<body>
    <div>
        <section id="activity-view">
            <h1>Activity</h1>
            <button onclick="toggleCreateActivity(true)">Create New Task</button>
            <ul>
                @if (count($activities)>0)
                    @foreach ($activities as $activity)
                        <li>
                            <form action="/toggle-done" method="get" class="done-form">
                                <input type="hidden" name="id" value = "{{$activity['id']}}">

                                <input type="checkbox" class="done-checkbox" name="checkbox" @if ($activity['completion_status'])
                                    checked
                                @endif>
                            </form>
                            {{$activity['header']}}
                            <button onclick="DisplayEditForm($activity['id'])">Edit ID: {{$activity['id']}}</button>
                        </li>
                    @endforeach
                @else
                    <p>No Activity Found</p>
                @endif
            </ul>
        </section>
        <section id="create-activity" style="display: none">
            <h1>Create Activity</h1>
            <button onclick="toggleCreateActivity(false)">Cancel</button>
            <form action="/create-activity" method="post">
                @csrf 
                <input type="text" placeholder="header" name="header"><br>
                <label for="dateInput">Date:</label>
                <input type="text" id="dateInput" name="deadline"><br>
                <textarea name="content" cols="45" rows="10"></textarea><br>
                <button type="submit">Submit</button>
            </form>
        </section>
        <section id="edit-activity" style="display: none">
            <h1>Masuk ke Sesi Edit Aktivitas</h1>
            <button onclick="toggleEditActivity(false)">Cancel</button>
            <form action="" method="post" id="edit-activity-forum">
                @csrf
                @method('put')
                <input type="hidden" id="activity-id-edit-form">

                <input type="text" placeholder="header" name="edit-header" id="header-edit-field"><br>
                <label for="dateInput">Date:</label>
                <input type="text" id="dateInput" name="edit-deadline" id="date-edit-field"><br>
                <textarea name="edit-content" cols="45" rows="10" id="content-edit-field"></textarea><br>
                
                <button type="submit" onclick="submitForm('delete')">Delete</button>
                <button type="submit" onclick="submitForm('edit')">Save Changes</button>
            </form>
        </section>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="/js/dateFormat.js"></script>
    <script src="/js/edit-activity.js"></script>
</body>
</html>
