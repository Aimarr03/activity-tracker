<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">
    <title>Log in to Attract!</title>
    
</head>
<body>
    <header>
        <div class="logo">
          <img src="assets/LOGO Doang.png" alt="Your Logo">
        </div>
        <div class="header-text">
          <h1 class="logo-text">Attract!</h2>
        </div>
    </header>
    <div class="wrapper text-center">
        <h1 class="login-title">Log in to <span class="logo-title">Attract!</span></h1>
        <form action="/login" method="post">
            @csrf
            <label class="label-text">Username</label>
            <input type="text" name="username">
            <label class="label-text">Password</label>
            <input type="password" name="password">
            <button type="submit">Let's Started</button>
        </form>
        <div class="create">
            Don't have an account? <a href="{{ route('signup.index') }}">Sign up</a>
        </div>
    </div>
</body>
</html> 