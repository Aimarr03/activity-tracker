<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubActivity extends Model
{
    use HasFactory;
    protected $fillable =['header', 'completion_status', 'activity_id'];
    protected $table = 'sub_activities';
    public function activity():BelongsTo{
        return $this->belongsTo(Activity::class);
    }
}
