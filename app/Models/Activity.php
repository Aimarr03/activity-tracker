<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Activity extends Model
{
    protected $fillable = ['header', 'deadline', 'content', 'completion_status', 'users_username', 'category_id'];
    protected $table = 'activities';

    public function user(): BelongsTo{
        return $this->belongsTo(User::class, 'users_username', 'username');
    }
    public function category():BelongsTo{
        return $this->belongsTo(Category::class);
    }
    public function sub_activity():HasMany{
        return $this->hasMany(SubActivity::class);
    }
    public function attachment():HasMany{
        return $this->hasMany(Picture::class);
    }
}
