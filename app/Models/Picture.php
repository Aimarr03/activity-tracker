<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Picture extends Model
{
    use HasFactory;
    protected $fillable = ['caption', 'picture'];

    public function activity():BelongsTo{
        return $this->belongsTo(Activity::class);
    }
}
