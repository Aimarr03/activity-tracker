<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements Authenticatable
{
    use HasFactory, AuthenticatableTrait;

    protected $fillable = ['username', 'password'];  
    protected $primaryKey = "username";
    protected $keyType = "string";

    public $incrementing = false;

    public function activity(): HasMany
    {
        return $this->hasMany(Activity::class);
    }
    public function category():HasMany{
        return $this->hasMany(Category::class);
    }
}
