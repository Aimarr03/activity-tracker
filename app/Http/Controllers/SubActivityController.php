<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\SubActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubActivityController extends Controller
{
    public function GetSubActivities($id){
        $sub_activities = SubActivity::where('activity_id', $id)->get();
        return $sub_activities;
    }
    public function CreateSubTask($id){
        $sub_task = SubActivity::create([
            'header' => 'new sub task',
            'completion_status' => false,
            'activity_id' => $id
        ]);
        $activity = Activity::where('id', $id)->first();
        if($activity){
            $sub_task->activity()->associate($activity);
        }
        $sub_task->save();

        return response()->json(['sub_task' => $sub_task]);
    }
    public function UpdateSubActivity(Request $request, $id){
        $content = $request->input('header');
        $sub_activity = SubActivity::find($id);
        $sub_activity->header = $content;

        $activity = Activity::where('id', $sub_activity->activity_id)->first();
        if($activity){
            $sub_activity->activity()->associate($activity);
        }
        $sub_activity->save();

        $sub_activities = SubActivity::where('activity_id', $activity->id)->get();
        return $sub_activities;
    }
    public function UpdateCheckBoxSubActivity(Request $request, $id){
        $completion_status = $request->input('completion_status');
        $sub_activity = SubActivity::find($id);
        $sub_activity->completion_status = $completion_status;

        $activity = Activity::where('id', $sub_activity->activity_id)->first();
        if($activity){
            $sub_activity->activity()->associate($activity);
        }
        $sub_activity->save();

        $sub_activities = SubActivity::where('activity_id', $activity->id)->get();
        return $sub_activities;
    }


    public function DeleteSubActivity($id){
        $sub_activity = SubActivity::find($id);
        $activity = Activity::where('id', $sub_activity->activity_id)->first();
        $sub_activity->delete();
        
        $sub_activities = SubActivity::where('activity_id', $activity->id)->get();
        return $activity->id;
    }
}
