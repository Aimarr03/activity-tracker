<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Activity;
use App\Models\Category;
use App\Models\SubActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    public function createActivity(Request $request){
        return $this->processActivityForm($request);
    }
    
    public function EditActivity(Request $request, $id){
        return $this->processActivityForm($request, $id);
    }
    public function GoBack(){
        return redirect('/');
    }
    private function processActivityForm(Request $request, $id = null) {
        $validatedData = $request->validate([
            'edit-header' => 'required',
            'edit-deadline' => 'required',
            'edit-content'=>'required',
            'category' => 'required',
        ]);
        $user = Auth::user();
        $category = Category::find($validatedData['category']);
    
        if ($id) {
            $activity = Activity::find($id);
        } else {
            $activity = new Activity();
        }
    
        $activity->header = $validatedData['edit-header'];
        $activity->deadline = $validatedData['edit-deadline'];
        $activity->content = $validatedData['edit-content'];
        $activity->completion_status = false;
        $activity->users_username = $user->username;
        $activity->category()->associate($category);
    
        $user = Auth::user();
        $user = User::where('username', $user->username)->first();
        if($user){
            $activity->user()->associate($user);
        }


        // $category = Category::where('id', 1)->first();
        // if ($category) {
        //     $activity->category()->associate($category);
        // }

        $activity->save();
    
        return redirect('/');
    }
    
    public function DeleteActivity($id){
        $activity = Activity::find($id);
        $activity->sub_activity()->delete();
        $activity->delete();
        return redirect('/');
    }
    public function ShowActivities()
    {
        $user = Auth::user();
        $activities = Activity::where('users_username', $user->username)->get();
        $categories = app(CategoryController::class)->GetCategories();

        return view('main_activity', ['activities' => $activities, 'categories' => $categories]);
        $user = Auth::user();
        $activities = Activity::where('users_username',$user->username)->get();
        $categories = app(CategoryController::class)->GetCategories();


        return view('main_activity', ['activities' => $activities, 'categories' => $categories]);
    }
    public function ShowDataActivity($id)
    {
        $activity = Activity::find($id);

        return response()->json(['activity' => $activity]);
    }
    public function GetActivityCategory($id){
        $activities = Activity::where('category_id', $id)->get();
        return $activities;
    }
    public function ToggleDone(Request $request){
        $id = $request->input('id');
        $checkbox = $request->input('checkbox');
        $checkbox = $checkbox ? 1 : 0;
        $activity = Activity::find($id);
        $activity->completion_status = $checkbox;
        $activity->save();
        return response()->json(['message' => 'Toggle done successfully', 'activity' => $activity]);
    }

    public function search(Request $request)
    {
        $keyword = $request->input('search');
        $activities = Activity::where('header', 'like', '%' . $keyword . '%')->get();
        return view('search', compact('activities', 'keyword'));
    }

    public function searchActivitiesByTime(Request $request, $timeCategory)
    {
        $keyword = $request->input('search');
        $category = $request->input('category', 'All'); // Default category is 'All'

        // Sesuaikan query berdasarkan kategori waktu
        $activities = Activity::when($timeCategory === 'Upcoming', function ($query) {
            return $query->where('deadline', '>', now());
        })
            ->when($timeCategory === 'Today', function ($query) {
                return $query->whereDate('deadline', now()->toDateString());
            })
            ->when($timeCategory === 'This Week', function ($query) {
                return $query->whereBetween('deadline', [now(), now()->endOfWeek()]);
            })
            ->when($category !== 'All', function ($query) use ($category) {
                return $query->where('category', $category);
            })
            ->when(!empty($keyword), function ($query) use ($keyword) {
                return $query->where('header', 'like', '%' . $keyword . '%');
            })
            ->get();

        $categories = app(CategoryController::class)->GetCategories();

        // Mengirim variabel $timeCategory ke tampilan Blade
        return view('activity_by_time', [
            'activities' => $activities,
            'categories' => $categories,
            'timeCategory' => $timeCategory,
        ]);
    }


    public function showActivitiesByTime($timeCategory)
    {
        $activities = [];

        switch ($timeCategory) {
            case 'Upcoming':
                $activities = Activity::where('users_username', Auth::user()->username)
                    ->where('deadline', '>', now())
                    ->get();
                break;

            case 'Today':
                $activities = Activity::where('users_username', Auth::user()->username)
                    ->whereDate('deadline', now()->toDateString())
                    ->get();
                break;

            case 'This Week':
                $activities = Activity::where('users_username', Auth::user()->username)
                    ->whereBetween('deadline', [now(), now()->endOfWeek()])
                    ->get();
                break;
        }

        $categories = app(CategoryController::class)->GetCategories();

        // Mengirim variabel $timeCategory ke tampilan Blade
        return view('activity_by_time', [
            'activities' => $activities,
            'categories' => $categories, 'timeCategory' => $timeCategory
        ]);
    }
}
