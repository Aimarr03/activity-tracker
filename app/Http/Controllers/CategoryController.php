<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function showCategories()
    {
    $categories = Category::all();
    return view('main_activity', ['categories' => $categories]);
    }
    public function GetCategories(){
        $user = Auth::user();
        $categories = Category::where('users_username',$user->username)->get();
        return $categories;
    }
    
    public function updateCategories(Request $request, $id){
        $categories = Category::find($id);
        $categories->category = $request->input('category');
        $categories->update();
    
        return response()->json(['message' => 'Category updated successfully']);
    }

    public function DeleteCategories($id){
        $category = Category::find($id);
        $activities = app(ActivityController::class)->GetActivityCategory($id);
        if($activities){
            foreach($activities as $activity){
                $activity->sub_activity()->delete();
                $activity->delete();
            }
        }
        if ($category->activity) {
            $category->activity->each(function ($activity) {
                $activity->sub_activity()->delete();
            });
    
            $category->activity()->delete();
        }
    
        $category->delete();
        
        return response()->json(['activities' => $activities]);
    }
    public function addCategory(Request $request){
        $user = Auth::user();
        $newCategory = Category::create([
            'category' => $request->category,
            'users_username' => $user->username
        ]);
        $newCategory->save();

        $categories = CategoryController::GetCategories();
        return response()->json(['message' => 'Category added successfully', 'categories' => $categories]);
    }
    public function ShowAllCategories(){
        $user = Auth::user();
        $categories = Category::with('activities')->where('users_username',$user->username)->get();
        return view('all_categories', ['categories' => $categories]);
    }
}