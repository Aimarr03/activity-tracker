<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function Login(Request $request){
        $validateForm = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if(Auth::attempt($validateForm)){
            $request->session()->regenerate();
            return redirect('/');
        }
        else{
            return redirect('/');
        }
    }
    public function Logout(){
        auth()->logout();
        return redirect('/');
    }
    public function Register(Request $request){
        $validateForm = $request->validate([
            "username" => ['required', 'min:3', Rule::unique('users','username')],
            'password' => ['required', 'min:8']
        ]);

        $validateForm['password'] = bcrypt($validateForm['password']);
        $user = User::create($validateForm);
        auth()->login($user);
        return redirect('/');
    }
    public function HomeFunction(){
        
    }
    public function signupRedirect() {
        return view('signup');
    }
    public function loginRedirect(){
        return view('login');
    }
}
