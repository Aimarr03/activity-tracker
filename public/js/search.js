$(document).ready(function() {
    $('#search-form').submit(function(e) {
        e.preventDefault();

        let searchTerm = $('#search-input').val();

        $.ajax({
            url: '/search',
            type: 'GET',
            data: { search: searchTerm },
            success: function(response) {
                $('#today-list').hide();
                $('#search-results').html(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    })
});
