function editCategory(categoryId) {
    const categoryChoice = $(
        `.category_choice[data-category-id=${categoryId}]`
    );
    const categoryText = categoryChoice.find(".category-text");
    const categoryInput = categoryChoice.find(".edit-category");
    const editButton = categoryChoice.find(".edit_button");
    const deleteButton = categoryChoice.find(".delete_button");

    categoryText.hide();
    categoryInput.show().focus();

    categoryInput.keyup(function (event) {});

    categoryInput.keypress(function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            const newCategoryName = $(this).val();
            const csrfToken = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                url: `/update-category/${categoryId}`,
                type: "PUT",
                data: {
                    category: newCategoryName,
                    _token: csrfToken,
                },
                success: function (response) {
                    categoryText.text(newCategoryName);
                    categoryText.show();
                    categoryInput.hide();

                    editButton.show();
                    deleteButton.show();
                    const categorySelect = $("#category-input");
                    categorySelect.find(`option[value="${categoryId}"]`).text(newCategoryName);
                },
                error: function (error) {
                    console.error("Error updating category:", error);
                },
            });
        }
    });

    editButton.hide();
    deleteButton.hide();
}

function deleteCategory(categoryId) {
    const categoryChoice = $(`.category_choice[data-category-id=${categoryId}]`);
    const csrfToken = $('meta[name="csrf-token"]').attr("content");

    $.ajax({
        url: `/delete-category/${categoryId}`,
        type: "DELETE",
        data: { _token: csrfToken },

        success: function (response) {
            console.log(response);
            categoryChoice.remove();
            const categorySelect = $("#category-input");
            categorySelect.find(`option[value="${categoryId}"]`).remove();
            
            var activityResponse = response.activities;
            activityResponse.forEach(element => {
                var activitiesHTML = document.getElementById(element.id);
                activitiesHTML.remove();
            });

            var activity_count = document.getElementById('activity_count');
            
            if (categorySelect.find("option").length === 0) {
                categorySelect.append('<option value="">No categories</option>');
            } 
            
            $.ajax({
                url: '/show-categories',
                type: 'GET',
                success: function (updatedCategories) {
                    var listHTML = document.getElementById('category-list');
                    listHTML.textContent = "";
                    if (updatedCategories.length === 0) {
                        var messageNoCategory = document.createElement('li');
                        messageNoCategory.textContent = 'No categories found';
                        listHTML.appendChild(messageNoCategory);
        
                        AddCategoryButton();
                    } else {
                        updatedCategories.forEach(category => {
                            AddNewCategoryHTML(category);
                        });
        
                        AddCategoryButton();
                    }
                    
                },
                error: function (error) {
                    console.error("Error fetching updated categories:", error);
                },
            });
        },           
        // error: function (error) {
        //     console.error("Error deleting category:", error);
        // },
    });
}

function addCategory() {
    const addCategoryContainer = $("#add-category-container");
    const addCategoryInput = $("#add-category-input");
    const categoryList = $("#category-list");

    addCategoryContainer.prop("disabled", true);

    addCategoryContainer.hide();
    addCategoryInput.show().focus();

    addCategoryInput.keypress('keyup',function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            const newCategoryName = $(this).val();
            const csrfToken = $('meta[name="csrf-token"]').attr("content");
            console.log('test');
            
            $.ajax({
                url: `/add-category`,
                type: "POST",
                data: {
                    category: newCategoryName,
                    _token: csrfToken,
                },
                success: function (response) {
                    $("#no-categories-message").hide();
                    console.log(response);
                    
                    var categories = response.categories;
                    if(categories.length === 0){
                        var messageNoCategory =document.createElement('li');
                        messageNoCategory.textContent = 'No categories found';
                        listHTML.appendChild(messageNoCategory);
                    }
                    var listHTML = document.getElementById('category-list');
                    listHTML.textContent = "";
                    categories.forEach(category => {
                        AddNewCategoryHTML(category);
                    });
                    
                    AddCategoryButton();
                    updateCategoryOptions(response.categories);

                },
                error: function (error) {
                    console.error("Error adding category:", error);
                },
                complete: function () {
                    addCategoryContainer.prop("disabled", false);
                },
            });
        }
    });
}

function updateCategoryOptions(categories) {
    var categorySelect = document.getElementById('category-input');
    categorySelect.innerHTML = "";

    categories.forEach(function(category) {
        var option = document.createElement('option');
        option.value = category.id;
        option.text = category.category;
        categorySelect.add(option);
    });
}

function AddNewCategoryHTML(category){
    const liElement = document.createElement('li');
    var container =  document.getElementById('category-list');
    liElement.classList.add('choice', 'category_choice');
    liElement.setAttribute('data-category-id', category.id);

    const categorySpan = document.createElement('span');
    categorySpan.classList.add('category-name');

    const categoryNameSpan = document.createElement('span');
    categoryNameSpan.classList.add('category-text');
    categoryNameSpan.textContent = category.category;

    // Create the input element for editing (initially hidden)
    const editInput = document.createElement('input');
    editInput.type = 'text';
    editInput.classList.add('edit-category');
    editInput.value = category.category;
    editInput.style.display = 'none';

    // Append the spans and input to the "category-name" span
    categorySpan.appendChild(categoryNameSpan);
    categorySpan.appendChild(editInput);

    // Create the div element with the class "option"
    const optionDiv = document.createElement('div');
    optionDiv.classList.add('option');

    // Create the "Edit" button
    const editButton = document.createElement('button');
    editButton.classList.add('edit_button');
    editButton.textContent = 'Edit';
    editButton.onclick = function() {
        editCategory(category.id);
    };

    // Create the "Delete" button
    const deleteButton = document.createElement('button');
    deleteButton.classList.add('delete_button');
    deleteButton.textContent = 'Delete';
    deleteButton.onclick = function() {
        deleteCategory(category.id);
    };


    optionDiv.appendChild(editButton);
    optionDiv.appendChild(deleteButton);

    liElement.appendChild(categorySpan);
    liElement.appendChild(optionDiv);

    container.appendChild(liElement);
}
function AddCategoryButton(){
    // Assuming there's a container element with the id "categoryList" where you want to append the new li element
    var container =  document.getElementById('category-list');

    // Create the li element for "Add New Category"
    const addCategoryLi = document.createElement('li');
    addCategoryLi.id = 'add-category';
    addCategoryLi.classList.add('choice');

    // Create the div element with the class "category-name" for the "Add New Category" container
    const addCategoryContainer = document.createElement('div');
    addCategoryContainer.id = 'add-category-container';
    addCategoryContainer.classList.add('category-name');
    addCategoryContainer.onclick = function() {
        addCategory();
    };

    // Create the "Add New Category" icon
    const plusIcon = document.createElement('i');
    plusIcon.classList.add('fa-solid', 'fa-plus', 'choice-logo');

    // Create the span element with the id "add-category-text" for the "Add New Category" text
    const addCategoryTextSpan = document.createElement('span');
    addCategoryTextSpan.id = 'add-category-text';
    addCategoryTextSpan.textContent = 'Add New Category';

    // Append the icon and text to the "category-name" div
    addCategoryContainer.appendChild(plusIcon);
    addCategoryContainer.appendChild(addCategoryTextSpan);

    // Create the input element for "Add New Category" (initially hidden)
    const addCategoryInput = document.createElement('input');
    addCategoryInput.type = 'text';
    addCategoryInput.id = 'add-category-input';
    addCategoryInput.style.display = 'none';

    // Append the "category-name" div and input to the li element
    addCategoryLi.appendChild(addCategoryContainer);
    addCategoryLi.appendChild(addCategoryInput);

    // Append the li element to the container
    container.appendChild(addCategoryLi);
}

function redirectToAllCategories() {
    window.location.href = '/show-all-categories';
}