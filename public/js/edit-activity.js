var sidebar = document.getElementById("sidebar");
var activity_view = document.getElementById("activity-view");
var activity_modify = document.getElementById("activity-modify");
var editButton = document.getElementById('edit');
var deleteButton = document.getElementById('delete');
var createButton = document.getElementById('create');

var menuBar = document.getElementsByClassName('menubar')[0];

var signoutButton = document.getElementsByClassName('sign-out')[0];
signoutButton.addEventListener('click', function(){
    document.getElementById('signoutForm').submit();
});

ToggleFormView(false, false);
ToggleSidebar(false);
function ToggleFormView(status, create){
    if(status){
        activity_modify.style.display = "flex";
        activity_view.classList.add('activity-view-collapse');
    }
    else{
        activity_modify.style.display = "none";
        activity_view.classList.remove('activity-view-collapse');
    }

    var isShowAllCategories = window.location.pathname === '/show-all-categories';

    if(create){
        editButton.style.display = "none";
        deleteButton.style.display = "none";
        createButton.style.display = "block";
    }
    else if(isShowAllCategories){
        editButton.style.display = "none";
        deleteButton.style.display = "none";
        createButton.style.display = "none";
    }
    else{
        editButton.style.display = "block";
        deleteButton.style.display = "block";
        createButton.style.display = "none";
    }
}
function ToggleSidebar(status){
    if(status){
        sidebar.style.left = '0px';
        sidebar.style.display = 'flex';
        menuBar.style.display = 'none';
        activity_view.classList.add('activity-view-full');
    }
    else{
        sidebar.style.left = '300px';
        sidebar.style.display = 'none';
        menuBar.style.display = 'block';
        activity_view.classList.add('activity-view-full');
    }
}

 

var sidebar = document.getElementById("sidebar");
var activity_view = document.getElementById("activity-view");
var activity_modify = document.getElementById("activity-modify");
var editButton = document.getElementById('edit');
var deleteButton = document.getElementById('delete');
var createButton = document.getElementById('create');

var menuBar = document.getElementsByClassName('menubar')[0];

var signoutButton = document.getElementsByClassName('sign-out')[0];
signoutButton.addEventListener('click', function(){
    document.getElementById('signoutForm').submit();
});

ToggleFormView(false, false);
ToggleSidebar(false);
function ToggleFormView(status, create){
    if(status){
        activity_modify.style.display = "flex";
        activity_view.classList.add('activity-view-collapse');
    }
    else{
        activity_modify.style.display = "none";
        activity_view.classList.remove('activity-view-collapse');
    }
    if(create){
        editButton.style.display = "none";
        deleteButton.style.display = "none";
        createButton.style.display = "block";
    }
    else{
        editButton.style.display = "block";
        deleteButton.style.display = "block";
        createButton.style.display = "none";
    }
}
function ToggleSidebar(status){
    if(status){
        sidebar.style.left = '0px';
        sidebar.style.display = 'flex';
        menuBar.style.display = 'none';
        activity_view.classList.add('activity-view-full');
    }
    else{
        sidebar.style.left = '300px';
        sidebar.style.display = 'none';
        menuBar.style.display = 'block';
        activity_view.classList.add('activity-view-full');
    }
}
var checkBoxes = document.querySelectorAll('.done-checkbox');
checkBoxes.forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
        var doneForm = this.closest('.done-form');
        var csrfToken = document.querySelector('input[name="_token"]').value;
        var checked = checkbox.checked ? 1 : 0;

        if (doneForm) {
            $.ajax({
                type: 'POST',
                url: '/toggle-done',
                dataType: 'json',
                data: {
                    id: doneForm.querySelector('input[name="id"]').value,
                    checkbox: checked,
                    _token: csrfToken
                },
                success: function (data) {
                    console.log("Data success toggled");
                    console.log(data);

                    // Update your UI based on the response data if needed
                    // For example, you can update the checkbox state or any other element
                },
                error: function (error) {
                    console.error("Error toggling data");
                    console.error(error);
                }
            });
        }
    });
});

// var checkBoxes = document.querySelectorAll('.done-checkbox');
// var doneForms = document.querySelectorAll('.done-form');
// checkBoxes.forEach(function(checkbox){
//     checkbox.addEventListener('change',function(){
//         var doneForm = this.closest('.done-form');
//         if (doneForm) {
//             doneForm.submit();
//         }
//     });
// });

var createActivity = document.getElementsByClassName('add-new-task')[0];
createActivity.addEventListener('click', function(){
    ToggleFormView(true, true);
    var input_header = document.getElementById('header-edit-field');
    var input_content = document.getElementById('content-edit-field');
    var input_deadline = document.getElementById('date-edit-field');

    input_header.value = "";

    input_content.value = "";
    input_content.textContent = "";
    
    input_deadline.value = "";
    SubActivityControllerJS.getSubActivity(0);
});

function DisplayEditForm(id){
    ToggleFormView(true, false);
    ToggleFormView(true, false);
    var Activity_ID = document.getElementById('activity-id-edit-form');
    Activity_ID.value = id;
    if(activity_modify.style.display != 'none'){
        $.ajax({
            type: 'GET',
            url: `/get-activity-data/${id}`,
            datatype: 'json',
            success: function(data){
                console.log(data.activity);
                console.log(data.activity.header);
                console.log(data.activity.deadline);
                console.log(data.activity.content);
                console.log(data.activity);

                // Set nilai dropdown kategori
                var categorySelect = document.getElementById('category-input');
                for (var i = 0; i < categorySelect.options.length; i++) {
                    if (categorySelect.options[i].value == data.activity.category_id) {
                        categorySelect.options[i].selected = true;
                        break;
                    }
                }

                $('#header-edit-field').val(data.activity.header);
                $('#date-edit-field').val(data.activity.deadline);
                $('#content-edit-field').val(data.activity.content);
                SubActivityControllerJS.getSubActivity(id);
                //console.log(data.sub_activities);
                // Log the values to check if they are set correctly
                //console.log($('#header-edit-field').val());
                //console.log($('#date-edit-field').val());
                //console.log($('#content-edit-field').val());

                // Log the values to check if they are set correctly
                console.log($('#header-edit-field').val());
                console.log($('#date-edit-field').val());
                console.log($('#content-edit-field').val());
            }
        });

        $('#edit-activity-forum').attr('action', `/edit-activity/${id}`);
    }
}

function toggleEditActivity(status){
    toggleCreateActivity(false);
    var EditActivity = document.getElementById('edit-activity');
    EditActivity.style.display = status ? 'block' : 'none';
    if(status){
        toggleCreateActivity(status);
    }
}
function toggleCreateActivity(status){
    var EditActivity = document.getElementById('create-activity');
    EditActivity.style.display = status ? 'block' : 'none';
}
function submitForm(type){
    var EditActivityForm = document.getElementById("edit-activity-forum");
    var Activity_ID = document.getElementById('activity-id-edit-form');
    switch(type){
        case 'delete':
            var methodForm = document.getElementsByName('_method')[0];
            methodForm.value = 'DELETE';
            EditActivityForm.action = `/delete-activity/${Activity_ID.value}`;
        break;
        case 'edit':
            EditActivityForm.action = `/edit-activity/${Activity_ID.value}`
        break;
        case 'create':
            var methodForm = document.getElementsByName('_method')[0];
            methodForm.value = 'post';
            EditActivityForm.action = `/create-activity`;
        break;
        case 'create':
            var methodForm = document.getElementsByName('_method')[0];
            methodForm.value = 'post';
            EditActivityForm.action = `/create-activity`;
        break;
    }
    EditActivityForm.submit();
}


function ToggleSearchForm() {
    const searchFormContainer = document.getElementById('search-form-container');
    searchFormContainer.style.display = searchFormContainer.style.display === 'none' ? 'block' : 'none';
}
