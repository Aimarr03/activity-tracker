var SubActivityControllerJS = SubActivityControllerJS || {}

document.getElementsByClassName('add-new-sub-task')[0].addEventListener('click', function () {
    var csrfToken = document.querySelector('input[name="_token"]').value;

    if (csrfToken) {
        // Make your AJAX request with the CSRF token
        var Activity_ID = document.getElementById('activity-id-edit-form');
        $.ajax({
            type: 'GET',
            url: `/create-new-sub-task/${Activity_ID.value}`,
            data: { _token: csrfToken },
            success: function (data) {
                console.log(data.sub_task);
                CreateSubTaskHTML(data.sub_task);
            },
            error: function (error) {
                console.error('Error:', error);
            }
        });
    } else {
        console.error('CSRF token not found.');
    }
});

function GetSubTask(id){
    $sub_task_list = document.getElementById('sub-task-list');
    var csrfToken = document.querySelector('input[name="_token"]').value;
    console.log(id);
    $.ajax({
        type: 'POST',
        url: `/get-sub-task/${id}`,
        data: { _token: csrfToken },
        datatype: 'json',
        success: function(data){
            console.log(data);
            $sub_task_list.innerHTML = "";
            for(var i = 0; i < data.length; i++){
                CreateSubTaskHTML(data[i]);
            }
            console.log('Display modified');
        }
    });
}
function CreateSubTaskHTML(data){
    // Create a new list item
    var listItem = document.createElement('li');

    // Create the structure for the sub-task
    var subTaskDiv = document.createElement('div');
    subTaskDiv.className = 'sub-task';

    var mainSubTaskSection = document.createElement('section');
    mainSubTaskSection.className = 'sub-task';
    mainSubTaskSection.id = 'sub-task-section-'+data['id'];
    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';

    checkbox.checked = data['completion_status'] === 1 ? true : false;

    checkbox.addEventListener('change', function(){
        var checkboxValue= checkbox.checked;
        var csrfToken = document.querySelector('input[name="_token"]').value;
        checkboxValue = checkboxValue ? 1 : 0;
        $.ajax({
            type: 'POST',
            url: `/edit-completion-status-subtask/${data['id']}`,
            data: { _token: csrfToken, completion_status: checkboxValue },
            datatype: 'json',
            success: function(data){
                GetSubTask(data[0]['activity_id']);
            }
        });
    });
    mainSubTaskSection.appendChild(checkbox);

    
    var title = document.createElement('h3');
    title.className = 'task-title';
    title.id = 'id-task-title-'+data['id'];
    console.log(data['header']);
    title.textContent = data['header'];
    
    mainSubTaskSection.appendChild(title);

    var buttonSection = document.createElement('section');
    buttonSection.className = 'sub-task button-sub-task-list';
    // var button = document.createElement('button');
    // button.innerHTML = '<i class="fa-solid fa-ellipsis-vertical expand-sub-task">' +
    //                     '<div class="sub-task-menu">' +
                        
    //                     '<h5 id="edit-sub-task">Edit</h5>' +
    //                     '<h5 id="delete-sub-task">Delete</h5>' +
    //                     '</div></i>';
    var editButton = document.createElement('button');
    editButton.textContent = "Edit";
    editButton.className = "edit-sub-task-button sub-task-button";
    editButton.id = 'edit-sub-task-' + data['id'];
    editButton.value = data['id'];
    editButton.onclick = function(){
        EditSubActivity(editButton.value);
    };

    var deleteButton = document.createElement('button');
    deleteButton.textContent = "Delete";
    deleteButton.classList = "delete-sub-task-button sub-task-button";
    deleteButton.id = 'delete-sub-task-' + data['id'];
    deleteButton.value = data['id'];
    deleteButton.onclick = function(){
        DeleteSubActivity(deleteButton.value);
    };
    
    buttonSection.appendChild(editButton);
    buttonSection.appendChild(deleteButton);
    

    subTaskDiv.appendChild(mainSubTaskSection);
    subTaskDiv.appendChild(buttonSection);

    // Create the input field
    var inputDiv = document.createElement('div');
    inputDiv.className = 'sub-task-input';
    var input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Title';
    inputDiv.appendChild(input);

    // Create the separator
    var separatorDiv = document.createElement('div');
    separatorDiv.className = 'seperator';

    // Append everything to the list item
    listItem.appendChild(subTaskDiv);
    listItem.appendChild(inputDiv);
    listItem.appendChild(separatorDiv);

    document.getElementById('sub-task-list').appendChild(listItem);
}
SubActivityControllerJS.getSubActivity = GetSubTask;

function EditSubActivity(id){
    var id_value = 'edit-sub-task-'+id;
    var class_section = 'sub-task-section-'+id;
    var title_id = 'id-task-title-'+id;
    
    var edit_button = document.getElementById(id_value);
    var title_sub_task = document.getElementById(title_id);

    var previous_content = title_sub_task.textContent;
    
    var input_button = document.createElement('input');
    var form_input = document.createElement('form');
    

    input_button.type = 'text';
    input_button.value = previous_content;
    input_button.className = 'sub-task-header-input';
    input_button.addEventListener('keyup', function(event){
        if(event.key === 'Enter'){
            ExecuteEditFunction(id, input_button.value);
        }
    });
    
    title_sub_task.parentNode.replaceChild(input_button, title_sub_task);
    
}
function DeleteSubActivity(id){
    var csrfToken = document.querySelector('input[name="_token"]').value;
    // console.log(id);
    $.ajax({
        type: 'POST',
        url: `/delete-sub-task/${id}`,
        data: {_token: csrfToken},
        datatype: 'json',
        success: function(data){
            console.log(data);
            console.log('delete successfully');
            GetSubTask(data);
        }
    });
}
function ExecuteEditFunction(id, new_header){
    var csrfToken = document.querySelector('input[name="_token"]').value;
    // console.log(id);
    // console.log(new_header);
    $.ajax({
        type: 'POST',
        url: `/update-sub-task/${id}`,
        data: { _token: csrfToken, header: new_header },
        datatype: 'json',
        success: function(data){
            GetSubTask(data[0]['activity_id']);
        }
    });
}