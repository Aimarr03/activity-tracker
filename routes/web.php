<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubActivityController;
use App\Models\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', [ActivityController::class, 'ShowActivities']);
Route::get('/', function () {
    if (Auth::check()) {
        //dd('Authenticated');
        return app(ActivityController::class)->ShowActivities();
    } else {
        return view('login');
    }
});
Route::get('/gotosignup', [UserController::class, 'signupRedirect'])->name('signup.index');
Route::get('/gotologin', [UserController::class, 'loginRedirect'])->name('login.index');

Route::post('/signup', [UserController::class, 'Register']);
Route::post('/login', [UserController::class, 'Login']);
Route::post('/logout', [UserController::class, 'Logout']);

Route::post('/create-activity', [ActivityController::class, 'CreateActivity']);
Route::get('/get-activity-data/{id}', [ActivityController::class, 'ShowDataActivity']);
Route::put('/edit-activity/{id}', [ActivityController::class, 'EditActivity']);
Route::post('/toggle-done', [ActivityController::class, 'ToggleDone']);
Route::delete('/delete-activity/{id}', [ActivityController::class, 'DeleteActivity']);

Route::get('/create-new-sub-task/{id}', [SubActivityController::class,'CreateSubTask']);
Route::match(['get', 'post'], '/get-sub-task/{id}', [SubActivityController::class, 'GetSubActivities']);
Route::match(['get','post'], '/update-sub-task/{id}', [SubActivityController::class, 'UpdateSubActivity']);
Route::match(['get','post'], '/delete-sub-task/{id}', [SubActivityController::class, 'DeleteSubActivity']);
Route::match(['get','post'], '/edit-completion-status-subtask/{id}', [SubActivityController::class, 'UpdateCheckBoxSubActivity']);

Route::get('/show-categories', [CategoryController::class, 'GetCategories'])->name('category.GetCategories');
Route::post('/add-category', [CategoryController::class, 'addCategory'])->name('category.addCategory');
Route::put('/update-category/{id}', [CategoryController::class, 'updateCategories'])->name('category.updateCategories');
Route::delete('/delete-category/{id}', [CategoryController::class, 'DeleteCategories'])->name('category.DeleteCategories');
Route::get('/search', [ActivityController::class, 'search'])->name('activities.search');
Route::get('/search/{timeCategory}', [ActivityController::class, 'searchActivitiesByTime'])->name('searchActivitiesByTime');

Route::get('/show-activities/{timeCategory}', [ActivityController::class, 'showActivitiesByTime'])->name('show-activities-by-time');
Route::get('/back', [ActivityController::class, 'GoBack']);
Route::get('/show-all-categories', [CategoryController::class, 'ShowAllCategories'])
    ->name('category.showAllCategories');