<?php 
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('activities', function(Blueprint $table){
            $table->id();
            $table->tinyText('header');
            $table->dateTime('deadline');
            $table->longText('content');
            $table->boolean('completion_status');
            
            // Add the foreign key column for the user relationship
            $table->string('users_username')->nullable(); // Adjust the data type if necessary
            
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('activities');
    }
};
